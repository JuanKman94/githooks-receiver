# frozen_string_literal: true

require "bundler/setup"
require "logger"
require "sinatra/base"
require "posix/spawn"
require "yaml"

module Githooks
  # Main log file, used by githooks to log operations & messages.
  #
  # Each deployment script may use a different log file.
  LOGGER_FILE = "log/hooks.log"

  APP_DIR = File.expand_path("..", __dir__)

  def self.settings_file
    return env_settings_file if File.exist?(env_settings_file)

    File.expand_path("../config/settings.yml", __dir__)
  end

  def self.env_settings_file
    File.expand_path("../config/settings.#{ENV["APP_ENV"]}.yml", __dir__)
  end
end

require_relative "githooks/middleware/token"
require_relative "githooks/app"
require_relative "githooks/launcher"
require_relative "githooks/errors/missing_token_error"
require_relative "githooks/errors/invalid_token_error"
