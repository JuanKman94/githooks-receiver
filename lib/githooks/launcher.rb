# frozen_string_literal: true

module Githooks
  class Launcher
    attr_accessor :logger

    def initialize(deployments:, logger:)
      @deployments = deployments
      @logger = logger
    end

    # Execute repo's deployment command.
    #
    # Only a subset of the environment variables are passed down to the child
    # process for various reasons: one being for better security and, secondly
    # because whenever a `bundle` starts a program (like self) it sets a bunch
    # of environment variables in the execution context, some being for
    # `ruby(1)`, others for `gem` and a lot more for `bundle` itself
    # (all starting with `BUNDLER_`).
    #
    # So whenever a deployment scripts uses `bundle` it's not using the system's
    # installation but the `githooks-receiver` bundle installation -- which is
    # wherever `BUNDLE_PATH` points to at the moment of execution and using the
    # project's Gemfile (read via environment variable `BUNDLE_GEMFILE`)

    def deploy_project(repo, data: {})
      cmd = @deployments[repo]
      cmd_options = {
        unsetenv_others: true
      }
      version = data&.fetch(:checkout_sha, "unknown")

      if cmd && ENV["APP_ENV"] != "test"
        begin
          pid = POSIX::Spawn.spawn(cmd_env, cmd, version, cmd_options)
          logger.info "[#{repo}] cmd = #{cmd} #{version} #{cmd_options}"
          Process.detach(pid)
        rescue SystemExit
          logger.error "[#{repo}] fork exited"
        end
      end
    end

    private

    # Environment variables passed down to child process
    #
    # == Passed variables
    # [DBUS_SESSION_BUS_ADDRESS] `DBUS_SESSION_BUS_ADDRESS`
    # [HOME] `HOME`
    # [LANG] `LANG`
    # [PATH] `BUNDLER_ORIG_PATH` or process' `PATH`
    # [PWD]  `PWD`
    # [USER] `USER`
    #
    # @return [Hash] Environment variables

    def cmd_env
      {
        "DBUS_SESSION_BUS_ADDRESS" => ENV["DBUS_SESSION_BUS_ADDRESS"],
        "HOME" => ENV["HOME"],
        "LANG" => ENV["LANG"],
        "PATH" => (ENV["BUNDLER_ORIG_PATH"] or ENV["PATH"]),
        "PWD" => ENV["PWD"],
        "USER" => ENV["USER"]
      }
    end
  end
end
