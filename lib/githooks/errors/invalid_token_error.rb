# frozen_string_literal: true

class Githooks::InvalidTokenError < RuntimeError
  def initialize(msg = "Invalid token, token does not match")
    super
  end
end
