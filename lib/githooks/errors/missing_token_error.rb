# frozen_string_literal: true

class Githooks::MissingTokenError < RuntimeError
  def initialize(msg = "Missing authorization token. Set it in ENV or .token")
    super
  end
end
