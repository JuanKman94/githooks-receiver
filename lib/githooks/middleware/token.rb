# frozen_string_literal: true

class TokenMiddleware < Sinatra::Base
  before do
    token = ENV["GITHOOKS_AUTH_TOKEN"]

    if !token && File.file?(".token")
      token = File.open(".token") { |f| f.readline.chomp }
    end

    raise Githooks::MissingTokenError if !token

    unless token == request.env["HTTP_X_GITLAB_TOKEN"]
      status 401
      halt JSON.dump({code: 401, status: "unauthorized"})
    end
  end
end
