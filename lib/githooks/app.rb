# frozen_string_literal: true

class Githooks::App < Sinatra::Base
  use TokenMiddleware

  configure do
    set(:port, ARGV.at(ARGV.index("-p") + 1)) if ARGV.include?("-p")
    set(:bind, ARGV.at(ARGV.index("-o") + 1)) if ARGV.include?("-o")
    set(:deployments, YAML.load_file(Githooks.settings_file, symbolize_names: true)[:deployments])
    set(:logger, Logger.new(Githooks::LOGGER_FILE))

    enable :logging
  end

  before do
    env["rack.logger"] = settings.logger
  end

  post "/repo/:repo" do
    repo = params["repo"].to_sym
    resp = {status: "ok", code: 200}

    begin
      # request.body.rewind
      logger.debug "params = #{params}"

      if !settings.deployments.key?(repo)
        logger.error "[#{repo}] invalid repo"
        status 404
        resp = {code: 404, status: "not_found"}
      else
        begin
          data = JSON.parse(request.body.read, symbolize_names: true)
        rescue JSON::ParserError
          data = params
        end

        logger.debug "[#{repo}] data = #{data}"
        launcher.deploy_project(repo, data: data)
      end
    rescue => err
      logger.error "unexpected error(#{err.class}): #{err}: #{err.backtrace.join("\n")}"
      status 500
      resp = {code: 500, status: "internal_server_error"}
    end

    body JSON.dump(resp)
  end

  # run the server **AFTER** defining the routes
  run! if app_file == $0

  def launcher
    @launcher ||= Githooks::Launcher.new(deployments: settings.deployments, logger: logger)
  end
end
