# frozen_string_literal: true

require "test_helper"

token = File.open(".token").readline.chomp if File.file?(".token")
token ||= "default-token"

ENV["GITHOOKS_AUTH_TOKEN"] = token
ENV["APP_ENV"] = "test"

def app
  Githooks::App
end

describe Githooks::Launcher do
  include Rack::Test::Methods

  let(:valid_repo) { "blog" }

  # Valid request means: valid token, valid repo & valid payload
  it "returns 200 OK with valid request" do
    payload = {sha_version: 1}

    header "X_GITLAB_TOKEN", token
    post "/repo/#{valid_repo}", JSON.dump(payload)

    assert last_response.ok?
    assert_equal 200, last_response.status
  end

  it "handles invalid payload" do
    payload = {sha_version: 1}

    header "X_GITLAB_TOKEN", token
    post "/repo/#{valid_repo}", payload

    assert last_response.ok?
    assert_equal 200, last_response.status
  end

  it "handles null payload" do
    payload = nil

    header "X_GITLAB_TOKEN", token
    post "/repo/#{valid_repo}", JSON.dump(payload)

    assert last_response.ok?
    assert_equal 200, last_response.status
  end

  it "returns 401 UNAUTHORIZED when invalid token" do
    post "/repo/invalid"

    assert_equal 401, last_response.status
  end

  it "returns 404 NOT FOUND when invalid repo" do
    header "X_GITLAB_TOKEN", token
    post "/repo/invalid"

    refute last_response.ok?
    assert_equal 404, last_response.status
  end

  it "returns 500 INTERNAL SERVER ERROR on unexpected error" do
    skip "TODO: add tests for sinatra"
  end
end
