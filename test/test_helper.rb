# frozen_string_literal: true

require "minitest/autorun"
require "minitest-spec-context"
require "rack/test"
require "debug"
require_relative "../lib/githooks"

ENV["RACK_ENV"] ||= "test"
