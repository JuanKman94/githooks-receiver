# Git Hooks Receiver

## External Resources

* [GitLab: Webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)
* [Apache HTTPD ProxyPass](https://httpd.apache.org/docs/2.4/mod/mod_proxy.html#proxypass)

## Dependencies

For fedora we have some dependencies to build with native extensions:

```bash
$ dnf install -y redhat-rpm-config ruby ruby-dev
$ dnf groupinstall -y "Development Tools"
$ bundle --path vendor/bundle
$ uuidgen > .token
```

## Deployments

It's important that if any of the deployment scripts rely on pulling changes
from a git repository using `ssh(1)`, the user **MUST** have access to said
repo. If the repository is private, the user executing the script **MUST** have
an SSH key **WITHOUT** a passphrase to use.

### Considerations

* If your deployment needs to restart a `systemd.unit(5)` service, `githooks`
needs to pass the `DBUS_SESSION_BUS_ADDRESS` environment variable to the
child process

## Server Configuration

We allow [httpd(8)](https://linux.die.net/man/8/httpd) to connect to the
githooks' sinatra server and add an user
[systemd.unit(5)](http://man7.org/linux/man-pages/man5/systemd.unit.5.html)
to automatically start the service.

```
# setsebool httpd_can_network_connect on
$ cp conf/etc/systemd/system/githooks.service $HOME/.config/systemd/user/githooks.service
$ systemctl --user daemon-reload
$ systemctl --user enable githooks
$ systemctl --user start githooks
```
