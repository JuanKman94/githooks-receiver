# frozen_string_literal: true

require_relative "lib/githooks"

run Githooks::App
